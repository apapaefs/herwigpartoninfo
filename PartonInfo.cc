// -*- C++ -*-
//
// PartonInfo.cc is a part of Herwig - A multi-purpose Monte Carlo event generator
// Copyright (C) 2002-2016 The Herwig Collaboration
//
// Herwig++ is licenced under version 2 of the GPL, see COPYING for details.
// Please respect the MCnet academic guidelines, see GUIDELINES for details.
//
//
// This is the implementation of the non-inlined, non-templated member
// functions of the PartonInfo class.
//

#include <cmath>
#include <fstream>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>

#include "PartonInfo.h"
#include "Herwig/Utilities/Histogram.h"
#include "ThePEG/Repository/EventGenerator.h"
#include "ThePEG/EventRecord/Particle.h"
#include "ThePEG/Vectors/ThreeVector.h"
#include "ThePEG/EventRecord/Event.h"
#include "ThePEG/PDT/EnumParticles.h"
#include "ThePEG/Interface/Switch.h"
#include "ThePEG/Interface/Reference.h"
#include "ThePEG/Repository/UseRandom.h"
#include "ThePEG/Interface/Parameter.h"
#include "ThePEG/Interface/ClassDocumentation.h"
#include "ThePEG/Persistency/PersistentOStream.h"
#include "ThePEG/Persistency/PersistentIStream.h"



using namespace std;
using namespace Herwig;

PartonInfo::PartonInfo() {}

IBPtr PartonInfo::clone() const {
  return new_ptr(*this);
}

IBPtr PartonInfo::fullclone() const {
  return new_ptr(*this);
}

void PartonInfo::persistentOutput(PersistentOStream & os) const {
}

void PartonInfo::persistentInput(PersistentIStream & is, int) {
}

/***********************************************************************
 * MAIN ANALYSIS FUNCTION
************************************************************************/
void PartonInfo::analyze(tEventPtr event, long, int, int) {

  ParticleVector intermediates(event->primarySubProcess()->outgoing());

  
  writeout(intermediates);
 

}

/***********************************************************************
 This is where the main analysis function ends
************************************************************************/

ClassDescription<PartonInfo> PartonInfo::initPartonInfo;
// Definition of the static class description member.

//The Following provides INTERFACES to the various settings of the analysis
void PartonInfo::Init() {

  static ClassDocumentation<PartonInfo> documentation
    ("The PartonInfo class has been created to write out the parton-level events that go into the event generation");
  
}


/***********************************************************************
 This function is executed at the START of the analysis
************************************************************************/
void PartonInfo::doinitrun() {
  //initialize Analysis handler
  AnalysisHandler::doinitrun();

  ofstream outstream_init;
  outstream_init.open(generator()->filename() + ".parton");
  outstream_init.close();

  //open the output stream
  //outstream->open(generator()->filename() + ".parton");
}

/***********************************************************************
 This function is executed at the END of the analysis
************************************************************************/
void PartonInfo::dofinish() {
  //finish Analysis handler
  AnalysisHandler::dofinish();

  //close the output stream
  //outstream->close();
}

void PartonInfo::writeout(ParticleVector intermediates) {
  ofstream outstream;
  outstream.open(generator()->filename() + ".parton", std::ios_base::app);
  Lorentz5Momentum pp(0.*GeV,0.*GeV,0.*GeV,0.*GeV,0.*GeV);
  for(unsigned int ww = 0; ww < intermediates.size(); ww++) {
    //cout << "intermediates[ww]->id()= " << intermediates[ww]->id() << endl;
    pp = intermediates[ww]->momentum();
    // outstream << intermediates[ww]->id() << "\t" << pp.x()/GeV << "\t" << pp.y()/GeV << "\t" << pp.z()/GeV << "\t" << pp.e()/GeV << std::endl;
    outstream << intermediates[ww]->id() << "\t" << pp.x()/GeV << "\t" << pp.y()/GeV << "\t" << pp.z()/GeV << "\t" << pp.e()/GeV << std::endl;
  }
  outstream << std::endl;
      
  outstream.close();													     
}
  
  
